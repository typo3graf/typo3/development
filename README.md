[![TYPO3 Version](https://img.shields.io/static/v1?label=TYPO3%20&message=v10.4&color=f49800&&logo=typo3&logoColor=f49800)](https://typo3.org)
[![Latest Stable Version](https://img.shields.io/packagist/v/t3graf/development?color=33a2d8&label=stable)](https://packagist.org/packages/t3graf/development)
[![Latest Unstable Version](https://img.shields.io/packagist/v/t3graf/development?include_prereleases&label=unstable)](https://packagist.org/packages/t3graf/development)
[![Total Downloads](https://img.shields.io/packagist/dt/t3graf/development?color=1081c1)](https://packagist.org/packages/t3graf/development)
[![License](https://img.shields.io/packagist/l/t3graf/development?color=498e7f)](https://gitlab.com/typo3graf/typo3/development/-/blob/master/LICENSE.md)
# TYPO3-Environmet für die Entwicklung von TYPO3-Erweiterungen.

# ACHTUNG! - NUR FÜR DEVELOPMENT #


## Installation
In einer DDEV Umgebung. In der Root composer.json:

Unter required-dev:
`"t3graf/development": "10.4`

Weitere TYPO3-Erweiterungen können anschließend mit Composer hinzugefügt werden.

z.B.: `composer require typo3/cms-felogin` oder `composer require-dev typo3/cms-felogin`
